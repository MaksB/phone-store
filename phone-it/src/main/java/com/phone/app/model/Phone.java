package com.phone.app.model;

import java.util.Objects;

public class Phone {

    private String phoneId;
    private String name;
    private String image;
    private String description;
    private Price price;

    public String getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(String phoneId) {
        this.phoneId = phoneId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return Objects.equals(phoneId, phone.phoneId) &&
                Objects.equals(name, phone.name) &&
                Objects.equals(image, phone.image) &&
                Objects.equals(description, phone.description) &&
                Objects.equals(price, phone.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneId, name, image, description, price);
    }
}
