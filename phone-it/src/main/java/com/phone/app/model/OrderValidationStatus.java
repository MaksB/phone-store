package com.phone.app.model;

public enum OrderValidationStatus {
    VALID, INVALID
}
