package com.phone.app.model;

public enum OrderConfirmationStatus {
    CONFIRM, DECLINE
}
