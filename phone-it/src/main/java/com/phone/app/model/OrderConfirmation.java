package com.phone.app.model;

public class OrderConfirmation {

    private Long id;
    private OrderConfirmationStatus status;
    private Order order;
    private Price orderPrice;

    public OrderConfirmation(OrderConfirmationStatus status, Order order, Price orderPrice) {
        this.status = status;
        this.order = order;
        this.orderPrice = orderPrice;
    }

    public OrderConfirmationStatus getStatus() {
        return status;
    }

    public void setStatus(OrderConfirmationStatus status) {
        this.status = status;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Price getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Price orderPrice) {
        this.orderPrice = orderPrice;
    }

    @Override
    public String toString() {
        return "OrderConfirmation{" +
                "status=" + status +
                ", order=" + order +
                ", orderPrice=" + orderPrice +
                '}';
    }
}
