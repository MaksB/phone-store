package com.phone.app.model;

import java.util.List;

public class Order {

    private Long id;
    private Customer customer;
    private List<Phone> orderBucket;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Phone> getOrderBucket() {
        return orderBucket;
    }

    public void setOrderBucket(List<Phone> orderBucket) {
        this.orderBucket = orderBucket;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customer=" + customer +
                ", orderBucket=" + orderBucket +
                '}';
    }
}
