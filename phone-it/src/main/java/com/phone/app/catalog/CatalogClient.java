package com.phone.app.catalog;

import com.phone.app.configuration.CatalogClientConfiguration;
import com.phone.app.model.Phone;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Headers({
        "Accept: application/json"
})
@FeignClient(name = "catalogClient", url = "${catalog.client.url}", configuration = CatalogClientConfiguration.class)
public interface CatalogClient{
    @GetMapping(value = "/phones")
    List<Phone> getPhoneCatalogByIds(@RequestParam(required = false) List<String> ids);
    @GetMapping(value = "/phone/{id}")
    Phone getPhoneById(@PathVariable String id);
}
