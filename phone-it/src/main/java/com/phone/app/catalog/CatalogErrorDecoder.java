package com.phone.app.catalog;

import com.phone.app.exception.PhoneNotFoundException;
import feign.FeignException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import static feign.FeignException.errorStatus;

public class CatalogErrorDecoder extends ErrorDecoder.Default {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogErrorDecoder.class);

    @Override
    public Exception decode(String methodKey, Response response) {
        FeignException exception = errorStatus(methodKey, response);

        if (HttpStatus.NOT_FOUND.value() == response.status()) {
            LOGGER.error("Catalog not found with status code: {}", response.status());
            return new PhoneNotFoundException(exception.getMessage());
        }
        return super.decode(methodKey, response);
    }
}