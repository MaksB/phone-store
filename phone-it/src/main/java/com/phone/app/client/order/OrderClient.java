package com.phone.app.client.order;

import com.phone.app.configuration.OrderClientConfiguration;
import com.phone.app.model.Order;
import com.phone.app.model.OrderConfirmation;
import com.phone.app.model.OrderValidation;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Headers({
        "Accept: application/json"
})
@FeignClient(name = "orderClient", url = "${order.client.url}", configuration = OrderClientConfiguration.class)
public interface OrderClient {
    @PostMapping(value = "/order/check")
    OrderValidation checkOrder(@RequestBody Order order);

    @PutMapping(value = "/order/create")
    OrderConfirmation createOrder(@RequestBody Order order);
}
