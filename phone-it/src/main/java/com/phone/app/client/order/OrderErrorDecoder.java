package com.phone.app.client.order;

import com.phone.app.exception.InvalidOrderException;
import feign.FeignException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import static feign.FeignException.errorStatus;

public class OrderErrorDecoder extends ErrorDecoder.Default {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderErrorDecoder.class);

    @Override
    public Exception decode(String methodKey, Response response) {
        FeignException exception = errorStatus(methodKey, response);

        if (HttpStatus.BAD_REQUEST.value() == response.status()) {
            LOGGER.error("Catalog not found with status code: {}", response.status());
            return new InvalidOrderException(exception.getMessage());
        }
        return super.decode(methodKey, response);
    }
}