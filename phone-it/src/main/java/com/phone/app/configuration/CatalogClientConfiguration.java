package com.phone.app.configuration;


import com.phone.app.catalog.CatalogClient;
import com.phone.app.catalog.CatalogErrorDecoder;
import feign.Logger;
import feign.Request;
import feign.Retryer;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(clients = CatalogClient.class)
public class CatalogClientConfiguration {

    @Value("${catalog.client.timeout.connection}")
    private Integer connectTimeOutMillis;
    @Value("${catalog.client.timeout.read}")
    private Integer readTimeOutMillis;
    @Value("${catalog.client.retry.period}")
    private Long retryPeriod;
    @Value("${catalog.client.retry.maxPeriod}")
    private Long retryMaxPeriod;
    @Value("${catalog.client.retry.maxAttempts}")
    private Integer retryMaxAttempts;
    @Value("${catalog.client.feign.log.level}")
    private String logLevel;

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.valueOf(logLevel);
    }

    @Bean
    public Request.Options options() {
        return new Request.Options(connectTimeOutMillis, readTimeOutMillis);
    }

    @Bean
    public Retryer.Default retryer() {
        return new Retryer.Default(retryPeriod, retryMaxPeriod, retryMaxAttempts);
    }
    @Bean
    public ErrorDecoder errorDecoder(){
        return new CatalogErrorDecoder();
    }
}
