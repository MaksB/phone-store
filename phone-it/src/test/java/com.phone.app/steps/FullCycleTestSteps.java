package com.phone.app.steps;

import com.phone.app.PhoneIntegrationTest;
import com.phone.app.TestContext;
import com.phone.app.catalog.CatalogClient;
import com.phone.app.client.order.OrderClient;
import com.phone.app.exception.InvalidOrderException;
import com.phone.app.exception.PhoneNotFoundException;
import com.phone.app.model.Customer;
import com.phone.app.model.Order;
import com.phone.app.model.OrderConfirmation;
import com.phone.app.model.OrderConfirmationStatus;
import com.phone.app.model.OrderValidation;
import com.phone.app.model.OrderValidationStatus;
import com.phone.app.model.Phone;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.util.List;

@EnableAutoConfiguration
public class FullCycleTestSteps extends PhoneIntegrationTest {

    @Autowired
    private CatalogClient catalogClient;
    @Autowired
    private OrderClient orderClient;
    @Autowired
    private TestContext testContext;

    @When("^get phone catalog$")
    public void getPhoneCatalog() throws Throwable {
        List<Phone> phones = catalogClient.getPhoneCatalogByIds(null);
        testContext.setPhones(phones);
    }

    @When("^get phone catalog with ids \"([^\"]*)\"$")
    public void getPhoneCatalog(String arg1) throws Throwable {
        List<Phone> phones = catalogClient.getPhoneCatalogByIds(List.of(arg1));
        testContext.setPhones(phones);
    }

    @When("^get phone by id \"([^\"]*)\"$")
    public void getPhoneById(String arg1) throws Throwable {
        Phone phone = null;
        try {
            phone = catalogClient.getPhoneById(arg1);
        }catch (Exception e){
            testContext.setException(e);
        }
        testContext.setPhone(phone);
    }

    @Then("^check if phone catalog is not empty$")
    public void checkIfCatalogIsNotEmpty() throws Throwable {
        Assert.assertFalse(testContext.getPhones().isEmpty());
    }

    @Then("^check if phone catalog is empty$")
    public void checkIfCatalogIsEmpty() throws Throwable {
        Assert.assertTrue(testContext.getPhones().isEmpty());
    }

    @Then("^check if phone is not null$")
    public void checkIfPhoneIsNotEmpty() throws Throwable {
        Assert.assertNotNull(testContext.getPhone());
    }

    @Then("^check if phone is null$")
    public void checkIfPhoneIsNull() throws Throwable {
        Assert.assertNull(testContext.getPhone());
    }

    @Then("^phone id is equals \"([^\"]*)\"$")
    public void checkIfPhoneIdIsEquals(String arg1) throws Throwable {
        Assert.assertEquals(arg1, testContext.getPhone().getPhoneId());
    }

    @And("^exception PhoneNotFoundException is present$")
    public void checkIfPhoneNotFoundExceptionIsPresent() throws Throwable {
        Assert.assertTrue(testContext.getException() instanceof PhoneNotFoundException);
    }
    @And("^exception InvalidOrderException is present$")
    public void checkIfInvalidOrderExceptionIsPresent() throws Throwable {
        Assert.assertTrue(testContext.getException() instanceof InvalidOrderException);
    }

    @Then("^check the new order$")
    public void checkNewOrder() throws Throwable {
       OrderValidation orderValidation = orderClient.checkOrder(testContext.getOrder());
       testContext.setOrderValidation(orderValidation);
    }

    @Then("^complete the new order$")
    public void completeNewOrder() throws Throwable {
       OrderConfirmation orderConfirmation = null;
        try {
            orderConfirmation = orderClient.createOrder(testContext.getOrderValidation().getOrder());
        }catch (Exception e){
            testContext.setException(e);
        }
        testContext.setOrderConfirmation(orderConfirmation);
    }

    @Then("^order validation status is equals \"([^\"]*)\"$")
    public void validateOrderValidationStatus(String arg1) throws Throwable {
        Assert.assertEquals(OrderValidationStatus.valueOf(arg1), testContext.getOrderValidation().getStatus());
    }

    @Then("^order complete status is equals \"([^\"]*)\"$")
    public void validateOrderStatus(String arg1) throws Throwable {
        Assert.assertEquals(OrderConfirmationStatus.valueOf(arg1), testContext.getOrderConfirmation().getStatus());
    }


    @And("^create new order with customer name \"([^\"]*)\" email \"([^\"]*)\"$")
    public void createNewOrder(String arg1, String arg2) throws Throwable {
        Order order = new Order();
        Customer customer = new Customer();
        customer.setName(arg1);
        customer.setEmail(arg2);
        order.setCustomer(customer);
        order.setOrderBucket(testContext.getPhones());
        testContext.setOrder(order);
    }

}
