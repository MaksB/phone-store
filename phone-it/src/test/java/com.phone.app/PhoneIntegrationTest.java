package com.phone.app;

import com.phone.app.configuration.CatalogClientConfiguration;
import com.phone.app.configuration.OrderClientConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

@ImportAutoConfiguration(FeignAutoConfiguration.class)
@SpringBootTest(classes = {CatalogClientConfiguration.class, OrderClientConfiguration.class})
@ContextConfiguration(classes = {TestContext.class})
public class PhoneIntegrationTest {

}
