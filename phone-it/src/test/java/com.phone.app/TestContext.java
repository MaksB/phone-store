package com.phone.app;

import com.phone.app.model.Order;
import com.phone.app.model.OrderConfirmation;
import com.phone.app.model.OrderValidation;
import com.phone.app.model.Phone;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope("cucumber-glue")
public class TestContext {

    private List<Phone> phones;
    private Order order;
    private Phone phone;
    private Exception exception;
    private OrderValidation orderValidation;
    private OrderConfirmation orderConfirmation;

    public List<Phone> getPhones() {
        return phones;
    }

    public Order getOrder() {
        return order;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public OrderValidation getOrderValidation() {
        return orderValidation;
    }

    public void setOrderValidation(OrderValidation orderValidation) {
        this.orderValidation = orderValidation;
    }

    public OrderConfirmation getOrderConfirmation() {
        return orderConfirmation;
    }

    public void setOrderConfirmation(OrderConfirmation orderConfirmation) {
        this.orderConfirmation = orderConfirmation;
    }
}
