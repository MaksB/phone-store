Feature: Order API

  Scenario Outline: Success cases
    When get phone catalog with ids "<phoneId>"
    Then check if phone catalog is not empty
    And create new order with customer name "<customer_name>" email "<email>"
    Then check the new order
    And order validation status is equals "<validationStatus>"
    Then complete the new order
    Then order complete status is equals "<orderStatus>"

    Examples:
      | phoneId                     | customer_name | email             | validationStatus | orderStatus |
      | 7b89971aa68fb6188023eac9    | testName      | testemail@gmail   |  VALID           | CONFIRM     |

  Scenario Outline: Unsuccess cases
    When get phone catalog with ids "<phoneId>"
    Then check if phone catalog is empty
    And create new order with customer name "<customer_name>" email "<email>"
    Then check the new order
    And order validation status is equals "<validationStatus>"
    Then complete the new order
    And exception InvalidOrderException is present

    Examples:
      | phoneId                     | customer_name | email             | validationStatus |
      | test                        | testName      | testemail@gmail   |  INVALID         |

