Feature: Catalog API

Scenario Outline: Success cases
  When get phone catalog
  Then check if phone catalog is not empty
  And get phone by id "<phoneId>"
  Then check if phone is not null
  And phone id is equals "<phoneId>"

  Examples:
  | phoneId                     |
  | 7b89971aa68fb6188023eac9    |


Scenario Outline: Unsuccess cases
  When get phone catalog with ids "<phoneId>"
  Then check if phone catalog is empty
  And get phone by id "<phoneId>"
  Then check if phone is null
  And exception PhoneNotFoundException is present

  Examples:
  | phoneId |
  | test    |




