# phone-store #

- [Introduction](#introduction)
- [catalog-service](#catalog-service)
- [order-service](#order-service)
- [Run Locally](#run-locally)
    * [Docker-Compose](#docker-compose)
    * [mysql](#mysql)
- [Development](#Development)
- [Questions](#Questions)

## Introduction

The goal of this project is to build the phone-store on the microservices architecture. 
The list microservices is:

1.  [catalog-service](#catalog-service) 
2.  [order-service](#order-service)

##catalog-service
 The main responsibility for this microservice is returning phones catalog via rest. This service required mongo database and using for running http port 8081.
 The url `http://localhost:8081/actuator/health` returns the health status for the microservice.
 This microservice expose the swagger documentation, use next url `http://localhost:8081/swagger-ui.html`
 The microservices expose the next endpoints:
 
 1.Return the phone list `curl -X GET "http://localhost:8081/phones" -H "accept: application/json"`
 
```
 Response body:
 [
   {
     "phoneId": "5b8d2208de8754ce3810a957",
     "name": "iphone 7",
     "image": "http:/aws.s3.com/ipnone/7",
     "description": "new iphone 7",
     "price": {
       "amount": 650,
       "currency": "USD"
     },
     "links": [
       {
         "rel": "self",
         "href": "http://localhost:8081/phone/5b8d2208de8754ce3810a957",
         "hreflang": null,
         "media": null,
         "title": null,
         "type": null,
         "deprecation": null
       }
     ]
   },
   . . . . . . 
  ]
```
 
2.Return the phone by id `curl -X GET "http://localhost:8081/phone/5b8d2208de8754ce3810a957" -H "accept: application/json"`

```
 Response body:
 {
   "phoneId": "5b8d2208de8754ce3810a957",
   "name": "iphone 7",
   "image": "http:/aws.s3.com/ipnone/7",
   "description": "new iphone 7",
   "price": {
     "amount": 650,
     "currency": "USD"
   },
   "_links": {
     "self": {
       "href": "http://localhost:8081/phone/5b8d2208de8754ce3810a957"
     }
   }
 }
```
 
 3.Return the phones by page `curl -X GET "http://localhost:8081/page/phones?offset=2&pageNumber=0&pageSize=5" -H "accept: application/json"`
 
```
 Response body:
{
  "content": [
. . . . . . . . . . .
  ],
  "pageable": {
    "sort": {
      "sorted": false,
      "unsorted": true
    },
    "offset": 0,
    "pageSize": 20,
    "pageNumber": 0,
    "unpaged": false,
    "paged": true
  },
  "totalPages": 1,
  "totalElements": 12,
  "last": true,
  "size": 20,
  "number": 0,
  "sort": {
    "sorted": false,
    "unsorted": true
  },
  "first": true,
  "numberOfElements": 12
}
```

 4.Return the phones by ids `curl -X GET "http://localhost:8081/phones?ids=5b8d2208de8754ce3810a957&ids=5b8d2208de8754ce3810a958" -H "accept: application/json"`
 
```
 Response body:
 [
   {
     "phoneId": "5b8d2208de8754ce3810a957",
     "name": "iphone 7",
     "image": "http:/aws.s3.com/ipnone/7",
     "description": "new iphone 7",
     "price": {
       "amount": 650,
       "currency": "USD"
     },
     "links": [
       {
         "rel": "self",
         "href": "http://localhost:8081/phone/5b8d2208de8754ce3810a957",
         "hreflang": null,
         "media": null,
         "title": null,
         "type": null,
         "deprecation": null
       }
     ]
   },
    {
      "phoneId": "5b8d2208de8754ce3810a958",
      "name": "Samsung s3",
      "image": "http:/aws.s3.com/samsung/s3",
      "description": "new Samsung s3",
      "price": {
        "amount": 350,
        "currency": "USD"
      },
      "links": [
        {
          "rel": "self",
          "href": "http://localhost:8081/phone/5b8d2208de8754ce3810a958",
          "hreflang": null,
          "media": null,
          "title": null,
          "type": null,
          "deprecation": null
        }
      ]
    }
 ]
```

##order-service
  The main responsibility for this microservice is checking and creating an order. This service required MySQL database and using for running http port 8082.
  The url `http://localhost:8082/actuator/health` returns the health status for the microservice.
  This microservice expose the swagger documentation, use next url `http://localhost:8082/swagger-ui.html`
  The microservices expose the next endpoints:
  
  1.Check the order `curl -X POST "http://localhost:8082/order/check" -H "accept: application/json" -H "Content-Type: application/json" -d`
  
```
  Request body:
 {
   "customer": {
     "email": "customer@gmail.com",
     "name": "Customer",
     "surname": "Customer"
   },
   "orderBucket": [
     {
       "phoneId": "5b8d2208de8754ce3810a957",
       "name": "iphone 7",
       "image": "http:/aws.s3.com/ipnone/7",
       "description": "new iphone 7",
       "price": {
         "amount": 650,
         "currency": "USD"
       },
       "links": [
         {
           "rel": "self",
           "href": "http://localhost:8081/phone/5b8d2208de8754ce3810a957"
         }
       ]
     },
     {
       "phoneId": "5b8d2208de8754ce3810a958",
       "name": "Samsung s3",
       "image": "http:/aws.s3.com/samsung/s3",
       "description": "new Samsung s3",
       "price": {
         "amount": 350,
         "currency": "USD"
       },
       "links": [
         {
           "rel": "self",
           "href": "http://localhost:8081/phone/5b8d2208de8754ce3810a958"
         }
       ]
     }
   ]
 }
```
  
```
  Response body:
  {
    "order": {
      "id": 3,
      "customer": {
        "name": "Customer",
        "surname": "Customer",
        "email": "customer@gmail.com"
      },
      "orderBucket": [
        {
          "id": 5,
          "phoneId": "5b8d2208de8754ce3810a957",
          "name": "iphone 7",
          "image": "http:/aws.s3.com/ipnone/7",
          "description": "new iphone 7",
          "price": {
            "amount": 650,
            "currency": "USD"
          }
        },
        {
          "id": 6,
          "phoneId": "5b8d2208de8754ce3810a958",
          "name": "Samsung s3",
          "image": "http:/aws.s3.com/samsung/s3",
          "description": "new Samsung s3",
          "price": {
            "amount": 350,
            "currency": "USD"
          }
        }
      ]
    },
    "message": "Order is valid",
    "status": "VALID"
  }
```
 
2.Create the order `curl -X PUT "http://localhost:8082/order/create" -H "accept: application/json" -H "Content-Type: application/json" -d`

```
  Request body:
   {
     "customer": {
       "email": "customer@gmail.com",
       "name": "Customer",
       "surname": "Customer"
     },
     "id": 4,
     "orderBucket": [
       {
         "id": 7,
         "phoneId": "5b8d2208de8754ce3810a957",
         "name": "iphone 7",
         "image": "http:/aws.s3.com/ipnone/7",
         "description": "new iphone 7",
         "price": {
           "amount": 650,
           "currency": "USD"
         }
       },
       {
         "id": 8,
         "phoneId": "5b8d2208de8754ce3810a958",
         "name": "Samsung s3",
         "image": "http:/aws.s3.com/samsung/s3",
         "description": "new Samsung s3",
         "price": {
           "amount": 350,
           "currency": "USD"
         }
       }
     ]
   }
```
   
```
   Response body:
   {
     "status": "CONFIRM",
     "order": {
       "id": 4,
       "customer": {
         "name": "Customer",
         "surname": "Customer",
         "email": "customer@gmail.com"
       },
       "orderBucket": [
         {
           "id": 7,
           "phoneId": "5b8d2208de8754ce3810a957",
           "name": "iphone 7",
           "image": "http:/aws.s3.com/ipnone/7",
           "description": "new iphone 7",
           "price": {
             "amount": 650,
             "currency": "USD"
           }
         },
         {
           "id": 8,
           "phoneId": "5b8d2208de8754ce3810a958",
           "name": "Samsung s3",
           "image": "http:/aws.s3.com/samsung/s3",
           "description": "new Samsung s3",
           "price": {
             "amount": 350,
             "currency": "USD"
           }
         }
       ]
     },
     "orderPrice": {
       "amount": 1000,
       "currency": "USD"
     }
   }
```
 
## Run Locally

Requirements
- docker-compose
- Java 10

1. Run the `./buildAndRun.sh`  
2. Run integration test after all docker services is up `./runItTest.sh` 

### Docker-Compose

Run the following command to start up the catalog-service and order-service
`docker-compose up --build`. Docker compose ups next services:
- mongodb
- mongo_init:
  - mongo_init, populate default phone catalog from [the file](docker_compose/db/mongo)
- mysql
- catalog-service
- order-service

### mysql

For clearing the MySQL data base firstly stop all docker containers `docker-compose down` and next run the command `docker system prune --force --volumes`


## Development

Requirements
- docker-compose
- Java 10

1. Run with docker-compose the next services with command `docker-compose up mongodb mongo_init mysql`
- mongodb
- mongo_init:
  - mongo_init, populate default phone catalog from [the file](docker_compose/db/mongo)
- mysql
2. Set spring active profile `local` to each services. Use the `--Dspring.profiles.active=local` Java VM options or set to application properties `spring:
                                                                                                                                                     profiles:
                                                                                                                                                       active: "local"`                   
### Questions
##- How would you improve the system?
1. I would like to add monitoring tools for this system like grafana etc. and application performance management tool
2. I would like to add some load balancer and discovery service (Zuul, Eureka) and spring cloud config server for reason to keep all properties in one place
3. I would like to use the Caffeine cache with auto refresh mechanism on the services and http client
4. The services need to be the enhancement with new endpoints for managing orders, phones
5. I would like to add BDD framework for testing each service independently
6. Will be cool to have deployment with kubernetes 

##- How would you avoid your order API to be overflow? 
For avoiding overflowing API I will use a Caffeine cache with auto refresh mechanism in the Feign Catalog client. Also will have load balancer and I will scale order-service.
Analise the code with time/performance metrics and found some bottlenecks, try to improve this code and if it possible run some logic parallel and asynchronous.                                                                                                                                                  
                                                                                                                                                       