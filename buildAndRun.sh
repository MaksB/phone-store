#!/bin/bash

echo "********* Build *********"

cd catalog-service
./gradlew clean build
if [ $? -eq 0 ]
then
  echo "Catalog-service build success"
else
  echo "Catalog-service Build failed" >&2
fi
cd ..
cd order-service
./gradlew clean build
if [ $? -eq 0 ]
then
  echo "Order-service build success"
else
  echo "Order-service build failed" >&2
fi
cd ..
echo "********* Run *********"


docker-compose up --build
                                 