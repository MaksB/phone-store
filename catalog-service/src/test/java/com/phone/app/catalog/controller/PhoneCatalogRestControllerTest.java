package com.phone.app.catalog.controller;

import com.phone.app.catalog.model.Phone;
import com.phone.app.catalog.model.Price;
import com.phone.app.catalog.service.PhoneCatalogService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public class PhoneCatalogRestControllerTest {

    private PhoneCatalogRestController phoneCatalogRestController;
    private PhoneCatalogService phoneCatalogService;

    @Before
    public void init() {
        phoneCatalogService = Mockito.mock(PhoneCatalogService.class);
        phoneCatalogRestController = new PhoneCatalogRestController(phoneCatalogService);
    }

    @Test
    public void getPhoneCatalogTest() {
        Mockito.when(phoneCatalogService.getAllPhones(Mockito.anyList())).thenReturn(List.of(getPhone()));

        var result = phoneCatalogRestController.getPhoneCatalog(List.of());

        Assert.assertNotNull(result);
        Assert.assertFalse(result.getBody().isEmpty());
    }

    @Test
    public void getPhoneCatalogByPageTest(){
        Mockito.when(phoneCatalogService.getAllPhones(Mockito.any(Pageable.class))).thenReturn(new PageImpl<>(List.of(getPhone())));

        var result = phoneCatalogService.getAllPhones(new PageRequest(0,1));

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(getPhone(), result.iterator().next());
    }

    @Test
    public void getPhoneTest(){
        Mockito.when(phoneCatalogService.getPhoneById(Mockito.anyString())).thenReturn(getPhone());

        var result = phoneCatalogService.getPhoneById("");

        Assert.assertNotNull(result);
        Assert.assertEquals(getPhone(), result);
    }


    private Phone getPhone() {
        var phone = new Phone();
        phone.setDescription("Test phone");
        phone.setImage("image");
        phone.setPhoneId("testId");
        phone.setPrice(new Price());
        return phone;
    }

}
