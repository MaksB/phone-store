package com.phone.app.catalog.service;

import com.phone.app.catalog.exception.PhoneNotFoundException;
import com.phone.app.catalog.model.Phone;
import com.phone.app.catalog.model.Price;
import com.phone.app.catalog.repository.PhoneCatalogRepository;
import com.phone.app.catalog.service.impl.PhoneCatalogServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class PhoneCatalogServiceImplTest {

    private PhoneCatalogService phoneCatalogService;
    private PhoneCatalogRepository phoneCatalogRepository;

    @Before
    public void init() {
        phoneCatalogRepository = Mockito.mock(PhoneCatalogRepository.class);
        phoneCatalogService = new PhoneCatalogServiceImpl(phoneCatalogRepository);
    }

    @Test
    public void getAllPhonesTest() {
        Mockito.when(phoneCatalogRepository.findAll()).thenReturn(List.of(getPhone()));

        var phones = phoneCatalogService.getAllPhones(List.of());

        Assert.assertNotNull(phones);
        Assert.assertEquals(1, phones.size());
        Assert.assertEquals(getPhone(), phones.iterator().next());
    }

    @Test
    public void getAllPhonesWithListIdsTest() {
        Mockito.when(phoneCatalogRepository.findByPhoneIdIn(Mockito.anyList())).thenReturn(List.of(getPhone()));

        var phones = phoneCatalogService.getAllPhones(List.of(""));

        Assert.assertNotNull(phones);
        Assert.assertEquals(1, phones.size());
        Assert.assertEquals(getPhone(), phones.iterator().next());
    }

    @Test
    public void getAllPhonesByPageableTest() {
        Mockito.when(phoneCatalogRepository.findAll(Mockito.any(Pageable.class))).thenReturn(new PageImpl<>(List.of(getPhone())));

        var phones = phoneCatalogService.getAllPhones(new PageRequest(0,1));

        Assert.assertNotNull(phones);
        Assert.assertEquals(1, phones.getTotalElements());
        Assert.assertEquals(getPhone(), phones.iterator().next());
    }

    @Test
    public void getPhoneByIdTest(){
        Mockito.when(phoneCatalogRepository.findById(Mockito.anyString())).thenReturn(Optional.of(getPhone()));

        var phone = phoneCatalogService.getPhoneById("");

        Assert.assertNotNull(phone);
        Assert.assertEquals(getPhone(), phone);
    }

    @Test(expected = PhoneNotFoundException.class)
    public void getPhoneByIdEmptyResponseTest(){
        Mockito.when(phoneCatalogRepository.findById(Mockito.anyString())).thenReturn(Optional.empty());

        phoneCatalogService.getPhoneById("");
    }

    private Phone getPhone() {
        var phone = new Phone();
        phone.setDescription("Test phone");
        phone.setImage("image");
        phone.setPhoneId("testId");
        phone.setPrice(new Price());
        return phone;
    }
}
