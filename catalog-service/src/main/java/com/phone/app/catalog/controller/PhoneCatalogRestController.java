package com.phone.app.catalog.controller;

import com.phone.app.catalog.model.Phone;
import com.phone.app.catalog.service.PhoneCatalogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(produces = "application/json")
public class PhoneCatalogRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneCatalogRestController.class);


    private PhoneCatalogService phoneCatalogService;

    @Autowired
    public PhoneCatalogRestController(PhoneCatalogService phoneCatalogService) {
        this.phoneCatalogService = phoneCatalogService;
    }

    @GetMapping(value = "/phones")
    public HttpEntity<List<Phone>> getPhoneCatalog(@RequestParam(required = false) List<String> ids) {
        LOGGER.info("Get phone catalog");
        List<Phone> phoneCatalogs = phoneCatalogService.getAllPhones(ids);
        phoneCatalogs.forEach(phoneCatalog ->
                phoneCatalog.add(linkTo(methodOn(PhoneCatalogRestController.class)
                        .getPhone(phoneCatalog.getPhoneId()))
                        .withSelfRel()));

        return new ResponseEntity<>(phoneCatalogs, HttpStatus.OK);
    }

    @GetMapping(value = "/page/phones")
    public HttpEntity<Page<Phone>> getPhoneCatalogByPage(Pageable pageable) {
        LOGGER.info("Get phone catalog");
        Page<Phone> phoneCatalogs = phoneCatalogService.getAllPhones(pageable);
        phoneCatalogs.forEach(phoneCatalog -> phoneCatalog.add(linkTo(methodOn(PhoneCatalogRestController.class)
                    .getPhone(phoneCatalog.getPhoneId()))
                    .withSelfRel()));
        return new ResponseEntity<>(phoneCatalogs, HttpStatus.OK);
    }

    @GetMapping(value = "/phone/{id}")
    public HttpEntity<Phone> getPhone(@PathVariable String id) {
        LOGGER.info("Get phone by catalogId={}", id);
        Phone phoneCatalog = phoneCatalogService.getPhoneById(id);
        phoneCatalog.add(linkTo(methodOn(PhoneCatalogRestController.class)
                .getPhone(id))
                .withSelfRel());
        return new ResponseEntity<>(phoneCatalog, HttpStatus.OK);
    }

}
