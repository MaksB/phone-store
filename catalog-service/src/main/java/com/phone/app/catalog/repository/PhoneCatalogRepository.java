package com.phone.app.catalog.repository;

import com.phone.app.catalog.model.Phone;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PhoneCatalogRepository extends MongoRepository<Phone, String> {

    List<Phone> findByPhoneIdIn(List<String> phoneIds);
}
