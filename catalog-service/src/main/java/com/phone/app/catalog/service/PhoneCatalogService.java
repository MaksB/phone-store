package com.phone.app.catalog.service;

import com.phone.app.catalog.model.Phone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PhoneCatalogService {
    List<Phone> getAllPhones(List<String> ids);
    Page<Phone> getAllPhones(Pageable pageable);
    Phone getPhoneById(String id);
}
