package com.phone.app.catalog.service.impl;

import com.phone.app.catalog.exception.PhoneNotFoundException;
import com.phone.app.catalog.model.Phone;
import com.phone.app.catalog.repository.PhoneCatalogRepository;
import com.phone.app.catalog.service.PhoneCatalogService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PhoneCatalogServiceImpl implements PhoneCatalogService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneCatalogServiceImpl.class);

    private PhoneCatalogRepository phoneCatalogRepository;

    @Autowired
    public PhoneCatalogServiceImpl(PhoneCatalogRepository phoneCatalogRepository) {
        this.phoneCatalogRepository = phoneCatalogRepository;
    }

    @Override
    public List<Phone> getAllPhones(List<String> ids) {
        List<Phone> phones;

        if (CollectionUtils.isNotEmpty(ids)) {
            LOGGER.debug("Load phones by ids={} from Mongo repository", ids);
            phones = phoneCatalogRepository.findByPhoneIdIn(ids);
        } else {
            LOGGER.debug("Load phones from Mongo repository");
            phones = phoneCatalogRepository.findAll();
        }
        return phones;
    }

    @Override
    public Page<Phone> getAllPhones(Pageable pageable) {
        LOGGER.debug("Load phones page={} from Mongo repository", pageable);
        return phoneCatalogRepository.findAll(pageable);
    }

    @Override
    public Phone getPhoneById(String id) {
        LOGGER.debug("Load phone by phoneId={} from Mongo repository", id);
        Optional<Phone> phone = phoneCatalogRepository.findById(id);
        if (!phone.isPresent()) {
            LOGGER.warn("Phone with phoneId={} doesn't exist", id);
            throw new PhoneNotFoundException(String.format("Catalog with catalogId=%s doesn't exist", id));
        }
        return phone.get();
    }
}
