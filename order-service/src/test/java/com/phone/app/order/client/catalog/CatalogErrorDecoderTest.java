package com.phone.app.order.client.catalog;

import com.phone.app.order.exception.PhoneNotFoundException;
import feign.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

public class CatalogErrorDecoderTest {

    private CatalogErrorDecoder catalogErrorDecoder;

    @Before
    public void init(){
        catalogErrorDecoder = new CatalogErrorDecoder();
    }

    @Test
    public void testDecodeExceptionFor404Response(){
        Exception exception = catalogErrorDecoder.decode("",createResponse(404));

        Assert.assertNotNull(exception);
        Assert.assertTrue(exception instanceof PhoneNotFoundException);
    }

    private Response createResponse(int httpStatus){
        return Response.builder().status(httpStatus).headers(new HashMap<>()).build();
    }
}
