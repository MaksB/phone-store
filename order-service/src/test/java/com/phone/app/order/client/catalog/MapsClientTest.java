package com.phone.app.order.client.catalog;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.phone.app.order.config.CatalogClientConfiguration;
import com.phone.app.order.exception.PhoneNotFoundException;
import com.phone.app.order.model.Phone;
import com.phone.app.order.model.Price;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

@RunWith(SpringRunner.class)
@AutoConfigureWireMock(port = 6065)
@ActiveProfiles("test")
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class })
@SpringBootTest(classes =  {CatalogClientConfiguration.class}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ImportAutoConfiguration(FeignAutoConfiguration.class)
public class MapsClientTest {

    private static final String PHONE_ID = "testEventId";

    @Autowired
    private CatalogClient catalogClient;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Value("${catalog.client.retry.maxAttempts}")
    private Integer retryMaxAttempts;

    @Before
    public void init() {
        WireMock.reset();
    }

    @Test
    public void testGetPhoneCatalogByIdsSuccess() throws JsonProcessingException {

        String successResponse = objectMapper.writeValueAsString(List.of(getPhone()));
        var listIds = List.of(PHONE_ID);

        stubFor(get(urlEqualTo("/phones?ids=" +listIds.stream().collect(Collectors.joining("&ids="))))
                .willReturn(aResponse().withHeader("Content-Type", "application/json").withBody(successResponse)));

        var result = catalogClient.getPhoneCatalogByIds(listIds);

        Assert.assertNotNull(result);
        Assert.assertEquals(result.iterator().next(), getPhone());
    }

    @Test
    public void testGetPhoneCatalogByIdsEmptyResponse() {
        var listIds = List.of(PHONE_ID);

        stubFor(get(urlEqualTo("/phones?ids=" +listIds.stream().collect(Collectors.joining("&ids="))))
                .willReturn(aResponse().withHeader("Content-Type", "application/json").withStatus(HttpStatus.SC_NO_CONTENT)));

        var result = catalogClient.getPhoneCatalogByIds(listIds);

        Assert.assertNull(result);
    }

    @Test(expected = PhoneNotFoundException.class)
    public void testGetPhoneCatalogByIdsDetailRetryFor404Response() {
        var listIds = List.of(PHONE_ID);

        stubFor(get(urlEqualTo("/phones?ids=" +listIds.stream().collect(Collectors.joining("&ids="))))
                .willReturn(aResponse().withHeader("Content-Type", "application/json").withStatus(HttpStatus.SC_NOT_FOUND)));

        catalogClient.getPhoneCatalogByIds(listIds);
    }

    private Phone getPhone() {
        var phone = new Phone();
        phone.setDescription("Test phone");
        phone.setImage("image");
        phone.setPhoneId("testId");
        phone.setPrice(new Price());
        return phone;
    }
}
