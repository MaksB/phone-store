package com.phone.app.order.service;

import com.phone.app.order.exception.InvalidOrderException;
import com.phone.app.order.model.Order;
import com.phone.app.order.model.OrderConfirmationStatus;
import com.phone.app.order.model.OrderValidationStatus;
import com.phone.app.order.model.Phone;
import com.phone.app.order.model.Price;
import com.phone.app.order.repository.OrderConfirmationRepository;
import com.phone.app.order.repository.OrderValidationRepository;
import com.phone.app.order.service.impl.OrderServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.List;


public class OrderServiceImplTest {

    private PhoneCatalogService phoneCatalogService;
    private OrderValidationRepository orderValidationRepository;
    private OrderConfirmationRepository orderConfirmationRepository;
    private OrderService orderService;
    private static final String PHONE_ID = "phoneId";
    private static final String PRICE = "300";

    @Before
    public void init(){
        phoneCatalogService = Mockito.mock(PhoneCatalogService.class);
        orderValidationRepository = Mockito.mock(OrderValidationRepository.class);
        orderConfirmationRepository = Mockito.mock(OrderConfirmationRepository.class);
        orderService = new OrderServiceImpl(phoneCatalogService, orderValidationRepository, orderConfirmationRepository);
    }

    @Test
    public void validateOrderTest(){
        Mockito.when(phoneCatalogService.getPhoneCatalog(Mockito.anyList())).thenReturn(List.of(getPhone(PHONE_ID)));

        var orderValidation = orderService.validateOrder(getOrder(PHONE_ID));

        Assert.assertNotNull(orderValidation);
        Assert.assertEquals(OrderValidationStatus.VALID, orderValidation.getStatus());
    }

    @Test
    public void validateOrderUnmatchedContentTest(){
        Mockito.when(phoneCatalogService.getPhoneCatalog(Mockito.anyList())).thenReturn(List.of(getPhone("")));

        var orderValidation = orderService.validateOrder(getOrder(PHONE_ID));

        Assert.assertNotNull(orderValidation);
        Assert.assertEquals(OrderValidationStatus.INVALID, orderValidation.getStatus());
    }

    @Test
    public void validateOrderPhoneDoesNotExistTest(){
        Mockito.when(phoneCatalogService.getPhoneCatalog(Mockito.anyList())).thenReturn(List.of());

        var orderValidation = orderService.validateOrder(getOrder(PHONE_ID));

        Assert.assertNotNull(orderValidation);
        Assert.assertEquals(OrderValidationStatus.INVALID, orderValidation.getStatus());
    }

    @Test
    public void createOrderTest(){
        Mockito.when(orderValidationRepository.existsByOrderIdAndStatus(Mockito.anyLong(),Mockito.any(OrderValidationStatus.class))).thenReturn(true);

        var OrderConfirmation = orderService.createOrder(getOrder(PHONE_ID));

        Assert.assertNotNull(OrderConfirmation);
        Assert.assertNotNull(OrderConfirmation.getOrderPrice());
        Assert.assertEquals(OrderConfirmationStatus.CONFIRM, OrderConfirmation.getStatus());
        Assert.assertEquals(new BigDecimal(PRICE), OrderConfirmation.getOrderPrice().getAmount());
    }

    @Test(expected = InvalidOrderException.class)
    public void createOrderWithoutValidationTest(){
        Mockito.when(orderValidationRepository.existsByOrderIdAndStatus(Mockito.anyLong(),Mockito.any(OrderValidationStatus.class))).thenReturn(false);

        orderService.createOrder(getOrder(PHONE_ID));
    }

    public Order getOrder(String phoneId){
        Order order = new Order();
        order.setId(1L);
        order.setOrderBucket(List.of(getPhone(phoneId)));
        return order;
    }

    private Phone getPhone(String id) {
        var phone = new Phone();
        phone.setDescription("Test phone");
        phone.setImage("image");
        phone.setPhoneId(id);
        Price price = new Price();
        price.setAmount(new BigDecimal(PRICE));
        phone.setPrice(price);
        return phone;
    }
}
