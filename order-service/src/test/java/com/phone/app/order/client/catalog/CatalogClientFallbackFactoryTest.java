package com.phone.app.order.client.catalog;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class CatalogClientFallbackFactoryTest {
    private CatalogClientFallbackFactory catalogClientFallbackFactory;

    @Before
    public void init(){
        catalogClientFallbackFactory = new CatalogClientFallbackFactory();
    }

    @Test
    public void createTest(){
        var result = catalogClientFallbackFactory.create(new Exception());

        Assert.assertNotNull(result);
        Assert.assertTrue(result.getPhoneCatalogByIds(List.of()).isEmpty());
    }
}
