package com.phone.app.order.service;

import com.phone.app.order.client.catalog.CatalogClient;
import com.phone.app.order.model.Phone;
import com.phone.app.order.service.impl.PhoneCatalogServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

public class PhoneCatalogServiceImplTest {

    private PhoneCatalogService phoneCatalogService;
    private CatalogClient catalogClient;

    @Before
    public void init(){
        catalogClient = Mockito.mock(CatalogClient.class);
        phoneCatalogService = new PhoneCatalogServiceImpl(catalogClient);
    }

    @Test
    public void getPhoneCatalogTest(){
        Mockito.when(catalogClient.getPhoneCatalogByIds(List.of())).thenReturn(List.of(new Phone()));

        var result = phoneCatalogService.getPhoneCatalog(List.of());

        Assert.assertNotNull(result);
        Assert.assertTrue(!result.isEmpty());
    }

}
