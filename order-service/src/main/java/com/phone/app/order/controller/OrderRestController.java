package com.phone.app.order.controller;

import com.phone.app.order.model.OrderConfirmation;
import com.phone.app.order.model.Order;
import com.phone.app.order.model.OrderValidation;
import com.phone.app.order.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/order", produces = "application/json")
public class OrderRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderRestController.class);

    @Autowired
    private OrderService orderService;

    @PostMapping(value = "/check")
    public HttpEntity<OrderValidation> checkOrder(@RequestBody Order order) {
        LOGGER.info("Check order={}", order);
        OrderValidation orderValidation = orderService.validateOrder(order);
        return new ResponseEntity<>(orderValidation, HttpStatus.OK);
    }

    @PutMapping(value = "/create")
    public HttpEntity<OrderConfirmation> createOrder(@RequestBody Order order) {
        LOGGER.info("Create order={}", order);
        OrderConfirmation orderConfirmation = orderService.createOrder(order);
        return new ResponseEntity<>(orderConfirmation, HttpStatus.OK);
    }

}
