package com.phone.app.order.model;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "userOrder")
public class Order {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @Embedded
    private Customer customer;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Phone> orderBucket;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Phone> getOrderBucket() {
        return orderBucket;
    }

    public void setOrderBucket(List<Phone> orderBucket) {
        this.orderBucket = orderBucket;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customer=" + customer +
                ", orderBucket=" + orderBucket +
                '}';
    }
}
