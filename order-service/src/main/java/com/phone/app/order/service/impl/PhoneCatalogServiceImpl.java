package com.phone.app.order.service.impl;

import com.phone.app.order.client.catalog.CatalogClient;
import com.phone.app.order.model.Phone;
import com.phone.app.order.service.PhoneCatalogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneCatalogServiceImpl implements PhoneCatalogService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneCatalogServiceImpl.class);

    private CatalogClient catalogClient;

    @Autowired
    public PhoneCatalogServiceImpl(CatalogClient catalogClient) {
        this.catalogClient = catalogClient;
    }

    @Override
    public List<Phone> getPhoneCatalog(List<String> ids) {
        LOGGER.debug("Retrieving phone from Catalog client");
        return catalogClient.getPhoneCatalogByIds(ids);
    }

}
