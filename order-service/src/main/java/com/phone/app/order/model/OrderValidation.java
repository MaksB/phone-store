package com.phone.app.order.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class OrderValidation {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Order order;
    private String message;
    private OrderValidationStatus status;

    public OrderValidation(String message, OrderValidationStatus status, Order order) {
        this.message = message;
        this.status = status;
        this.order = order;
    }

    public static OrderValidation successOrderValidationResponse(Order order){
        return new OrderValidation("Order is valid", OrderValidationStatus.VALID, order);
    }

    public String getMessage() {
        return message;
    }

    public OrderValidationStatus getStatus() {
        return status;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "OrderValidation{" +
                "message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}
