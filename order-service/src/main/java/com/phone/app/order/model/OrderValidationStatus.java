package com.phone.app.order.model;

public enum OrderValidationStatus {
    VALID, INVALID
}
