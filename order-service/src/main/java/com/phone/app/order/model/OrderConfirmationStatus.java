package com.phone.app.order.model;

public enum OrderConfirmationStatus {
    CONFIRM, DECLINE
}
