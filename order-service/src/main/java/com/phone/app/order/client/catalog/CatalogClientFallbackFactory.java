package com.phone.app.order.client.catalog;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CatalogClientFallbackFactory implements FallbackFactory<CatalogClient> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogClientFallbackFactory.class);

    @Override
    public CatalogClient create(Throwable cause) {
        return ids -> {
            LOGGER.warn("Handling fallback for getPhoneCatalogByIds");
            return List.of();
        };
    }
}
