package com.phone.app.order.service;

import com.phone.app.order.model.OrderConfirmation;
import com.phone.app.order.model.Order;
import com.phone.app.order.model.OrderValidation;

public interface OrderService {
    OrderValidation validateOrder(Order order);
    OrderConfirmation createOrder(Order order);
}
