package com.phone.app.order.repository;

import com.phone.app.order.model.OrderValidation;
import com.phone.app.order.model.OrderValidationStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderValidationRepository  extends CrudRepository<OrderValidation, Long> {
    Boolean existsByOrderIdAndStatus(Long orderId, OrderValidationStatus status);
}
