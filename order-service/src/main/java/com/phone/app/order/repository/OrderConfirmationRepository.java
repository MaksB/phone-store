package com.phone.app.order.repository;

import com.phone.app.order.model.OrderConfirmation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderConfirmationRepository extends CrudRepository<OrderConfirmation, Long> {
}
