package com.phone.app.order.service.impl;

import com.phone.app.order.exception.InvalidOrderException;
import com.phone.app.order.model.OrderConfirmation;
import com.phone.app.order.model.Order;
import com.phone.app.order.model.OrderConfirmationStatus;
import com.phone.app.order.model.OrderValidation;
import com.phone.app.order.model.OrderValidationStatus;
import com.phone.app.order.model.Phone;
import com.phone.app.order.model.Price;
import com.phone.app.order.repository.OrderConfirmationRepository;
import com.phone.app.order.repository.OrderValidationRepository;
import com.phone.app.order.service.OrderService;
import com.phone.app.order.service.PhoneCatalogService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneCatalogServiceImpl.class);

    private PhoneCatalogService phoneCatalogService;
    private OrderValidationRepository orderValidationRepository;
    private OrderConfirmationRepository orderConfirmationRepository;

    @Autowired
    public OrderServiceImpl(PhoneCatalogService phoneCatalogService,
                            OrderValidationRepository orderValidationRepository,
                            OrderConfirmationRepository orderConfirmationRepository) {
        this.phoneCatalogService = phoneCatalogService;
        this.orderValidationRepository = orderValidationRepository;
        this.orderConfirmationRepository = orderConfirmationRepository;
    }

    @Override
    public OrderValidation validateOrder(Order order) {
        OrderValidation orderValidation;
        try {
            List<String> phoneIds = extractPhoneIds(order);

            List<Phone> phones = phoneCatalogService.getPhoneCatalog(phoneIds);


            var phoneCheckResult = phones.stream()
                    .flatMap(phone -> order.getOrderBucket()
                            .stream()
                            .filter(orderValue -> orderValue.getPhoneId().equals(phone.getPhoneId()))
                            .filter(ordered -> checkPhoneContent(phone, ordered)))
                   .count();

            if (phoneCheckResult != phoneIds.size()) {
                LOGGER.warn("The Order={} doesn't meet the original phone catalog", order);
                throw new InvalidOrderException("The Order doesn't meet the original phone catalog");
            }

            orderValidation = OrderValidation.successOrderValidationResponse(order);
        } catch (InvalidOrderException exception) {
            orderValidation = new OrderValidation(exception.getMessage(), OrderValidationStatus.INVALID, order);
        }
        orderValidationRepository.save(orderValidation);
        return orderValidation;
    }

    @Override
    public OrderConfirmation createOrder(Order order) {
        validateOrderStatus(order);

        var orderConfirmation = new OrderConfirmation(OrderConfirmationStatus.CONFIRM, order, calculatePrice(order));

        orderConfirmationRepository.save(orderConfirmation);
        LOGGER.info("The final order={}", orderConfirmation);
        return orderConfirmation;
    }

    private Price calculatePrice(Order order) {
        var price = new Price();
        price.setAmount(order.getOrderBucket().
                stream()
                .map(Phone::getPrice)
                .map(Price::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add));
        return price;
    }

    private void validateOrderStatus(Order order) {
        Optional.ofNullable(orderValidationRepository
                .existsByOrderIdAndStatus(order.getId(), OrderValidationStatus.VALID))
                .filter(Boolean::booleanValue)
                .orElseThrow(() -> new InvalidOrderException("Validate order before creation"));
    }

    private boolean checkPhoneContent(Phone original, Phone ordered) {
        if (ordered.equals(original)) {
            return true;
        } else {
            LOGGER.warn("The ordered phone=%s doesn't accord the original phone=%s from catalog", ordered, original);
            throw new InvalidOrderException(String.format("The ordered phone=%s doesn't accord the original phone=%s from catalog", ordered, original));
        }
    }

    private List<String> extractPhoneIds(Order order) {
        return Optional.ofNullable(order)
                .map(Order::getOrderBucket)
                .filter(Objects::nonNull)
                .flatMap(phones -> Optional.of(phones
                        .stream()
                        .map(Phone::getPhoneId)
                        .distinct()
                        .collect(Collectors.toList())))
                .filter(CollectionUtils::isNotEmpty)
                .orElseThrow(() -> new InvalidOrderException("The Order can't be empty"));
    }
}
