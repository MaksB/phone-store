package com.phone.app.order.service;

import com.phone.app.order.model.Phone;

import java.util.List;

public interface PhoneCatalogService {
    List<Phone> getPhoneCatalog(List<String> ids);
}
