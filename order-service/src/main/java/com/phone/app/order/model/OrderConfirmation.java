package com.phone.app.order.model;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class OrderConfirmation {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private OrderConfirmationStatus status;
    @OneToOne(cascade = CascadeType.MERGE)
    private Order order;
    @Embedded
    private Price orderPrice;

    public OrderConfirmation(OrderConfirmationStatus status, Order order, Price orderPrice) {
        this.status = status;
        this.order = order;
        this.orderPrice = orderPrice;
    }

    public OrderConfirmationStatus getStatus() {
        return status;
    }

    public void setStatus(OrderConfirmationStatus status) {
        this.status = status;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Price getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Price orderPrice) {
        this.orderPrice = orderPrice;
    }

    @Override
    public String toString() {
        return "OrderConfirmation{" +
                "status=" + status +
                ", order=" + order +
                ", orderPrice=" + orderPrice +
                '}';
    }
}
