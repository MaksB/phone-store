package com.phone.app.order.client.catalog;

import com.phone.app.order.config.CatalogClientConfiguration;
import com.phone.app.order.model.Phone;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Headers({
        "Accept: application/json"
})
@FeignClient(name = "catalogClient", url = "${catalog.client.url}", configuration = CatalogClientConfiguration.class, fallbackFactory = CatalogClientFallbackFactory.class)
public interface CatalogClient{
    @GetMapping(value = "/phones")
    List<Phone> getPhoneCatalogByIds(@RequestParam List<String> ids);
}
